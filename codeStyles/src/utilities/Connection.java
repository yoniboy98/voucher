package utilities;

import java.sql.SQLException;

public interface Connection {
        java.sql.Connection getConnection() throws SQLException;

        void closeConnection();
    }


