package dao;

import domain.Client;
import domain.Orders;
import domain.Theme;
import domain.Voucher;
import utilities.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class OrderDAO {
    private Connection connection;

    public OrderDAO(Connection connection) {
        this.connection = connection;
    }

    public ArrayList<Orders> findAllOrders() throws SQLException {
        ArrayList<Orders> orders = new ArrayList<>();
        java.sql.Connection c = connection.getConnection();
        PreparedStatement statement = c.prepareStatement("select " +
                "orders.id,voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme" +
                ",voucher.name,voucher.information,voucher.supplier,voucher.price,voucher.valid_until," +
                "client.id,client.name,client.firstname,client.phonenumber,client.email," +
                "orders.quantity,orders.date " +
                "from orders orders" +
                " inner join voucher voucher" +
                " on voucher.id = orders.voucherID " +
                "inner join theme theme " +
                "on theme.id = voucher.themeID " +
                "inner join client client " +
                "on orders.clientID = client.id;");


        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                orders.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("nothing found");
        }
        set.close();
        connection.closeConnection();
        return orders;
    }


    private Orders setToData(ResultSet set) throws SQLException {
        return new Orders(
                set.getInt("orders.id"),
                new Voucher(
                        set.getInt("voucher.id"),
                        set.getString("voucher.vouchernr"),
                        new Theme(
                                set.getInt("theme.id"),
                                set.getString("theme.theme")
                        ),
                        set.getString("voucher.name"),
                        set.getString("voucher.information"),
                        set.getString("voucher.supplier"),
                        set.getDouble("voucher.price"),
                        set.getDate("voucher.valid_until")
                ),
                new Client(
                        set.getInt("client.id"),
                        set.getString("client.name"),
                        set.getString("client.firstname"),
                        set.getString("client.phonenumber"),
                        set.getString("client.email")
                ), set.getInt("orders.quantity"),
                set.getDate("orders.date")
        );
    }
}
