package dao;

import domain.Theme;
import domain.Voucher;
import utilities.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VoucherDAO {
    private Connection connection;

    public VoucherDAO(Connection connection) {
        this.connection = connection;
    }


    public ArrayList<Voucher> findAllVouchers() throws SQLException {
        ArrayList<Voucher> vouchers = new ArrayList<>();
        java.sql.Connection c = connection.getConnection();
        PreparedStatement statement = c.prepareStatement("select voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme," +
                "voucher.name,voucher.information," +
                "voucher.supplier,voucher.price,voucher." +
                "valid_until " +
                "from voucher voucher" +
                " inner join theme theme;");


        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                vouchers.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("nothing found");
        }
        set.close();

        connection.closeConnection();

        return vouchers;
    }

    private Voucher setToData(ResultSet set) throws SQLException {
        return new Voucher(
                set.getInt("voucher.id"),
                set.getString("voucher.vouchernr"),
                new Theme(
                        set.getInt("theme.id"),
                        set.getString("theme.theme")
                ),
                set.getString("voucher.name"),
                set.getString("voucher.information"),
                set.getString("voucher.supplier"),
                set.getDouble("voucher.price"),
                set.getDate("voucher.valid_until")
        );
    }
}
