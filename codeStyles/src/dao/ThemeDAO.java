package dao;

import domain.Theme;
import utilities.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ThemeDAO {
    private Connection connection;

    public ThemeDAO(Connection connection) {
        this.connection = connection;
    }


    public ArrayList<Theme> findAllThemes() throws SQLException {
        ArrayList<Theme> themes = new ArrayList<>();
        java.sql.Connection c = connection.getConnection();
        PreparedStatement statement = c.prepareStatement("select theme.id,theme.theme from theme theme;");


        ResultSet set = statement.executeQuery();
        if (set.next()) {
            Theme theme = new Theme(1, "e");
            theme.setId(set.getInt("theme.id"));
            theme.setTheme(set.getString("theme.theme"));
            themes.add((Theme) set);
        } else {
            try {
                throw new SQLException("nothing found");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }return themes;

    }}


