package dao;

import domain.Client;
import utilities.Connection;

import java.sql.*;
import java.util.ArrayList;


public class ClientDAO {
    private Connection connection;

    public ClientDAO(Connection connection) throws SQLException {
        this.connection = connection;
    }

    public ArrayList<Client> findAllClients() throws SQLException {
        ArrayList<Client> clients = new ArrayList<>();
        java.sql.Connection c = connection.getConnection();
        PreparedStatement statement = c.prepareStatement("select client.id,client.name,client.firstname,client.phonenumber,client.email from client client;");


        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                clients.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("nothing found");
        }
        set.close();
        connection.closeConnection();
        return clients;
    }

    private Client setToData(ResultSet set) throws SQLException {
        return new Client(
                set.getInt("client.id"),
                set.getString("client.name"),
                set.getString("client.firstname"),
                set.getString("client.phonenumber"),
                set.getString("client.email")
        );
    }
}
