package application;

import dao.ClientDAO;

import dao.OrderDAO;
import dao.ThemeDAO;
import dao.VoucherDAO;
import domain.Client;
import domain.Orders;
import domain.Theme;
import domain.Voucher;
import utilities.Connection;

import utilities.MySQLConnection.MySQLConnectionManager;

import java.util.ArrayList;
import java.util.List;

class RunnerApp {
                public static void main(String[] args) {
                    Connection cm = null;
                    try {
                        cm = (Connection) new MySQLConnectionManager("root", "");

                        ClientDAO clientDAO = new ClientDAO(cm);
                        System.out.println(clientDAO.toString());


                        ArrayList<Client> clients = clientDAO.findAllClients();
                        printMyList(clients);

                        VoucherDAO voucherDAO = new VoucherDAO(cm);
                        System.out.println(voucherDAO.toString());


                        ArrayList<Voucher>vouchers = voucherDAO.findAllVouchers();
                        printMyList(vouchers);

                        OrderDAO orderDAO = new OrderDAO(cm);
                        ArrayList<Orders> orders = orderDAO.findAllOrders();
                        printMyList(orders);
                        System.out.println(orders.toString());

                        ThemeDAO themeDAO = new ThemeDAO(cm);
                        ArrayList<Theme> theme = themeDAO.findAllThemes();
                        printMyList(theme);
                        System.out.println(theme.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (cm != null) {
                            cm.closeConnection();
                        }
                    }
                }

                private static void printMyList(List<?> list){
                    for(Object open : list){
                        System.out.println(open.toString());
                    }
                }
            }

